use std::env;
use std::io::{Error, ErrorKind};

fn main() -> Result<(), Error> {
    let args: Vec<String> = env::args().collect();
    let args = &args[1..];
    let range_min: i32;
    let range_max: i32;

    println!("{:#?}", args);

    if args.is_empty() {
        print_usage();

        return Err(Error::new(ErrorKind::InvalidInput, "Missing maximum range argument"));
    } else if args.len() == 1 {
        range_min = 0;
        range_max = args.get(0).unwrap().parse().unwrap();
    } else if args.len() == 2 {
        range_min = args.get(0).unwrap().parse().unwrap();
        range_max = args.get(1).unwrap().parse().unwrap();
    } else {
        print_usage();

        return Err(Error::new(ErrorKind::InvalidInput, "Unrecognized arguments"));
    }

    for i in range_min..range_max {
        println!("{}", get_fizz_buzz(i));
    };

    Ok(())
}

fn get_fizz_buzz(value: i32) -> String {
    if value % 15 == 0 {
        "FizzBuzz".to_string()
    } else if value % 5 == 0 {
        "Buzz".to_string()
    } else if value % 3 == 0 {
        "Fizz".to_string()
    } else {
        value.to_string()
    }
}

fn print_usage() {
    let usage: String = r#"
        Usage:
        cargo run [range_min] <range_max>
    "#.to_string();

    println!("{}", usage);
}

#[cfg(test)]
mod test {
    use crate::get_fizz_buzz;

    #[test]
    fn get_fizz_buzz_returns_number() {
        // Given
        let value: i32 = 1;
        let expected: String = value.to_string();

        // When
        let actual: String = get_fizz_buzz(value);

        // Then
        assert_eq!(expected, actual);
    }

    #[test]
    fn get_fizz_buzz_returns_fizz() {
        // Given
        let value: i32 = 3;
        let expected: String = "Fizz".to_string();

        // When
        let actual: String = get_fizz_buzz(value);

        // Then
        assert_eq!(expected, actual);
    }

    #[test]
    fn get_fizz_buzz_returns_buzz() {
        // Given
        let value: i32 = 5;
        let expected: String = "Buzz".to_string();

        // When
        let actual: String = get_fizz_buzz(value);

        // Then
        assert_eq!(expected, actual);
    }

    #[test]
    fn get_fizz_buzz_returns_fizzbuzz() {
        // Given
        let value: i32 = 15;
        let expected: String = "FizzBuzz".to_string();

        // When
        let actual: String = get_fizz_buzz(value);

        // Then
        assert_eq!(expected, actual);
    }
}
